﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medic : MonoBehaviour
{
    Transform target;

    [SerializeField]
    [Header("Сколько здаровья прибавить для викинга.")]
    int valueHealth;

    private void Start()
    {
        target = FindObjectOfType<Viking>().transform;
    }

    private void Update()
    {
        RadarDistanse();
    }


    //если герой подошел ближе чем 1 метр (задано хардкодом)
    //берем здоровье
    public void RadarDistanse()
    {
        if(!target)
        {
            return;
        }
        if(Vector3.Distance(transform.position, target.transform.position) <= 1 /*метр*/ )
        {

            if(
                //проверка чтобы не брать зоровье если у героя здоровье на максимуме
                target.gameObject.GetComponent<Viking>().GetCurrentHealth()==
                target.gameObject.GetComponent<Viking>().GetMaxHealth())
            {
                return;
            }

            target.gameObject.GetComponent<Viking>().SetCurrentHealth(valueHealth);

            Destroy(gameObject);
        }
    }
}
