﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



/// <summary>
/// отвечает за индикатор здоровья героя
/// </summary>
public class HealthIndicator : MonoBehaviour
{

    [SerializeField]
    Image imageHealth;

    
    Viking viking;
   
    void Start()
    {
        viking = FindObjectOfType<Viking>();
    }
    
    void Update()
    {
        if(!viking)
        {
            viking = FindObjectOfType<Viking>();
        }

        SetValueHealthIndicator();
    }


    //задает значение в UI елементе который отвечает за отображение здоровья викинга
    public void SetValueHealthIndicator()
    {

        //
        float x = (float)viking.GetCurrentHealth() / (float)viking.GetMaxHealth();
        
        //плавное изменение значения
        imageHealth.fillAmount = Mathf.MoveTowards(imageHealth.fillAmount, x, Time.deltaTime / 2);

    }
}
