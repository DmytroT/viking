﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Monster : MonoBehaviour
{
    NavMeshAgent navMeshAgent;
    Transform targetViking;
    Animator _animator;

    bool isDead;

    [SerializeField]
    int maxHealth;

    [SerializeField]
    int currentHealth;

    [SerializeField]
    GameObject splashBlood;

    [SerializeField]
    GameObject healthGenerate;

    Spawner spawner;

    UIManager uiManager;

    bool isAttack;

    //ссылка на точку в руке монстра где есть триггер который
    //в нужный момент включается при атаке
    // и метод онтрггерэнтер (так же и у героя)
    [SerializeField]
    PointDamage pointDamage;
    void Start()
    {
        //генерим индикатор здоровья для  омнстра
        uiManager = FindObjectOfType<UIManager>();

        GameObject healthBar = Instantiate(Resources.Load<GameObject>("SliderHealthMNonster"), transform.position, Quaternion.identity) as GameObject;
        healthBar.GetComponent<HealthMonster>().parentEnemy = this;



        _animator = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();

        targetViking = FindObjectOfType<Viking>().transform;

        //сразу в старте пусть бежит к цели (к викингу)
        if(navMeshAgent)
        {
            navMeshAgent.SetDestination(targetViking.position);
            SetAnim(EStateAnimMonster.Run);
        }

        spawner = FindObjectOfType<Spawner>();
    }

    void Update()
    {
        if(isDead)
        {
            return;
        }

        //монстр пусть стоит на месте в Idle если нет героя
        if(!targetViking || targetViking.GetComponent<Viking>().isDead)
        {
            SetAnim(EStateAnimMonster.Idle);
            navMeshAgent.SetDestination(transform.position);
            return;
        }

        //если герой уже рядом то атакет мостр героя
        if(Vector3.Distance(transform.position, targetViking.transform.position) <= 2.5f)
        {
            //чтобы не "дрифтовал" по инерции (отключам навмешаген - монстр резко останавливается 
            //а потом снова включаем)
            navMeshAgent.enabled = false;
            navMeshAgent.enabled = true;

            //поворачиваем монстра к герою
            MonsterLookAtTargetAxisZ();

            Attaka();
        }
        else
        {
            SetAnim(EStateAnimMonster.Run);
            navMeshAgent.SetDestination(targetViking.position);
        }
    }

    public void SetActiveSplashBlood()
    {
        splashBlood.SetActive(true);
        // StartCoroutine(CorSetActiveSplashBlood());
    }

    public IEnumerator CorSetActiveSplashBlood()
    {


        yield return new WaitForSeconds(10);

        //splashBlood.SetActive(false);
    }

    /// <summary>
    /// поворот по одной оси чтобы монст поворачавался к герою при атаке
    /// </summary>
    public void MonsterLookAtTargetAxisZ()
    {
        Vector3 direct = (targetViking.position - transform.position).normalized;

        Quaternion lookRotate = Quaternion.LookRotation(new Vector3(direct.x, 0, direct.z));

        transform.rotation = Quaternion.Lerp(transform.rotation, lookRotate, Time.deltaTime * 10);
    }


    public void Attaka()
    {
        if(isAttack)
        {
            return;
        }

        StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        isAttack = true;
        SetAnim(EStateAnimMonster.Attack);

        StartCoroutine(pointDamage.Attack());


        yield return new WaitForSeconds(2.5f);
        isAttack = false;
    }

    //установка уровня жизни монтстра
    //с проверками 
    //если параметр с минусом то это урон, если с плюсом то это
    //добавление жизни как и у героя
    public void SetCurrentHealth(int damage)
    {
        currentHealth += damage;

        //чтобы жизнь не вылезла за пределы
        if(currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }

        //если нет жизни то метод смерти
        if(currentHealth <= 0)
        {
            Death();
        }
        else
        {
            StopCoroutine(Attack());
            SetAnim(EStateAnimMonster.Damage);
        }

    }

    //гэттэр (типа свойства)
    public int GetMaxHealth()
    {
        return maxHealth;
    }
    //сэттэр(типа свойства)
    public int GetCurrentHealth()
    {
        return currentHealth;
    }
    /// <summary>
    /// Смерть монстра
    /// </summary>
    public void Death()
    {
        //sliderHealth.GetComponent<HealthMonster>().DestroyHealth();




        isDead = true;
        //удаляем navMeshAgent чтобы монстр не бегал мертвым по карте
        Destroy(navMeshAgent);
        Destroy(GetComponent<Collider>());

        //
        StaticCounter.SetCounter(1);//counter += 1;



        //номера в перечисления срвпадают со значениями в переходах в аниматор контроллере 
        //(4 это переход к анимации смерти)
        SetAnim(EStateAnimMonster.Die);




        //вызываем корутину (для задержки перед удалением) чтобы
        //анимация смерти успела проиграть.
        // можно было сделать событиями в анимации (в самом клипе)
        StartCoroutine(CorytDeath());


    }

    /// <summary>
    /// Метод изменения анимации
    /// </summary>
    public void SetAnim(EStateAnimMonster eStateAnimMonster)
    {
        _animator.SetInteger("StateAnim", (int)eStateAnimMonster);
    }

    //для задержки перед удалением монстра
    IEnumerator CorytDeath()
    {


        yield return new WaitForSeconds(5);

        //генерим монстра нового
        if(spawner)
        {
            // Debug.Log("SpawnObject");

            spawner.SpawnObject();
        }

        Instantiate(healthGenerate, transform.position, Quaternion.identity);


        Destroy(gameObject);


    }

    //для уничтожения "магических чисел". Здесь каждое значение 
    //совпадает со значением перехода в аниматор контроллере.
    //напрример: для перехода в Run нажно поменять значение 
    //в аниматор контроллере на  и т. д.
    public enum EStateAnimMonster
    {
        Idle = 0,
        Run = 1,
        Attack = 2,
        Damage = 3,
        Die = 4
    }
}
