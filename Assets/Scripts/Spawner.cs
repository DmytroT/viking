﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    Transform[] pointInstanceMonster;
    
    GameObject centerRadius;

    [SerializeField]
    float radiusSpawn;

    [SerializeField]
    GameObject objForSpawn;

    [SerializeField]
    GameObject monster;


    /// <summary>
    /// генерим одного монстра с учетом положения героя
    /// Бросаем луч из "неба" в "землю". с рандомным расстоянием по X и Z 
    /// с заданим диапазоном от героя
    /// </summary>
    public void SpawnObject()
    {
        if(!centerRadius)
        {
            centerRadius = FindObjectOfType<Viking>().gameObject;

            return;
        }
       
        RaycastHit hit;

        //луч от переданного центра  летит вниз и при столконовении генерит врага
        Ray ray = new Ray(new Vector3(
               centerRadius.transform.position.x + Random.Range(-radiusSpawn, radiusSpawn),
               centerRadius.transform.position.y + 20,
               centerRadius.transform.position.z + Random.Range(-radiusSpawn, radiusSpawn))
            , -transform.up);

        Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue);


        if(Physics.Raycast(ray, out hit))
        {
            if(hit.collider.GetComponent<Terrain>())
            {
                Instantiate(objForSpawn, hit.point, Quaternion.identity);
            }
        }
    }

    /// <summary>
    /// генерим 10 монстров сразу
    /// </summary>
    public void InstantiateMonster()
    {
        for(int i = 0; i < pointInstanceMonster.Length; i++)
        {
            Instantiate(monster, pointInstanceMonster[i].transform.position, Quaternion.identity);
        }
    }


    public void SpawnerRestart()
    {
        ///Удаляем всех монстров
        ///при проигрыше для подготовки сцены для новой игры
        Monster[] arryMonster = FindObjectsOfType<Monster>();
        
        for(int i = 0; i < arryMonster.Length; i++)
        {
            Destroy(arryMonster[i].gameObject);
        }
        
        ///Удаляем всех шары со здоровьем
        ///при проигрыше для подготовки сцены для новой игры
        Medic[] arryMedic = FindObjectsOfType<Medic>();

        for(int i = 0; i < arryMedic.Length; i++)
        {
            Destroy(arryMedic[i].gameObject);
        }
    }
}
