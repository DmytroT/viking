﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//
public class HealthMonster : MonoBehaviour
{
    public Monster parentEnemy;
    private Slider slider;

    void Start()
    {
        slider = GetComponent<Slider>();

        transform.SetParent(GameObject.FindObjectOfType<UIManager>().transform);
    }

    void Update()
    {
        if(!parentEnemy)
        {
            return;
        }


        //если монст умер то удаляем индикатор
        if(parentEnemy.GetCurrentHealth() <= 0)
        {
            DestroyHealthBar(true);
        }


        //
        SetValueHealthIndicator();


        //чтобы над головой монстра висел индикатор здоровьяя
        Vector2 position = Camera.main.WorldToScreenPoint(parentEnemy.transform.position);
        position.y += 80F;

        transform.position = position;
    }
    public void SetValueHealthIndicator()
    {

        float x = (float)parentEnemy.GetCurrentHealth() / (float)parentEnemy.GetMaxHealth();


        slider.value = Mathf.MoveTowards(slider.value, x, Time.deltaTime / 2);

    }


    /// <summary>
    /// для удаления всех инжикаторв здоровья при смерти игрока или смерти монстра
    /// </summary>
    /// <param name="isTime"></param>
    public void DestroyHealthBar(bool isTime)
    {
        ///с задержкой в 3 секунды когда монстр умирает 
        if(isTime == true)
        {
            Destroy(gameObject, 3);
        }
        else
        {
            //когда герой умариет для выкашивания всехх хелсбаров 
            //чтобы подготовить сцену для новой игры
            Destroy(gameObject);
        }


    }
}
