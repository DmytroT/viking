﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// не используется этот скрипт но пусть будет в проекте
/// </summary>
public class CameraController : MonoBehaviour
{
    Transform target;

    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    public bool LookAtPlayer = false;

    public bool RotateAroundPlayer = true;

    public float RotationsSpeed = 5.0f;
    
    void Start()
    {
        target = FindObjectOfType<PointForCamera>().transform;

        _cameraOffset = transform.position - target.position;
    }

    private void OnEnable()
    {
        target = FindObjectOfType<PointForCamera>().transform;
    }

    void LateUpdate()
    {
        if(RotateAroundPlayer)
        {
            Quaternion camTurnAngle =
                Quaternion.AngleAxis(Input.GetAxis("Mouse X") * RotationsSpeed, Vector3.up);


            //Quaternion camTurnAngle1 =
            //    Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * RotationsSpeed, Vector3.right);
            //_cameraOffset = camTurnAngle1 * _cameraOffset;


            _cameraOffset = camTurnAngle * _cameraOffset;


        }

        Vector3 newPos = target.position + _cameraOffset;

        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);

        if(LookAtPlayer || RotateAroundPlayer)
        {
            transform.LookAt(target);
        }
    }
}
