﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Viking : MonoBehaviour
{//
    private Animator _animator;

    private CharacterController _characterController;

    //ссылка на топор в руке
    //добавлен point в скелет
    // в иерархии где есть триггер . этим тригером и наносим урон.
    //он активируется в момент атаки а потом отключатся
    [SerializeField]
    AxeManager axeManager;

    public float Speed = 5.0f;

    public float RotationSpeed = 240.0f;

    private float Gravity = 20.0f;

    private Vector3 _moveDir = Vector3.zero;

    //для UI панели 
    //активируем соответствующее меню при смерти.
    UIManager uiPanel;

    int powerAttacka = -100;

    [SerializeField]
    int maxHealth = 20;
    //CharacterController characterController;
    //Vector3 move;
    //float jumpValue = 2f;
    //float h, v;
    //float gravity = 5;
    //public float rotationValue = 90.0F;

    private bool isAttack;
    bool isDamage;
    public bool isDead;

    [SerializeField]
    int curretnHealth = 75;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _characterController = GetComponent<CharacterController>();
        uiPanel = FindObjectOfType<UIManager>();
    }

    void Update()
    {
        if(Time.timeScale == 0.0f)
        {
            return;
        }

        ///////////////////////////////////////////////////////////////////////
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Attaka();
        }
        ///////////////////////////////////////////////////////////////////////

        

        Move();
    }


    /// <summary>
    /// ///если хотим на нести урон то передаем -1 -2 -3 ...  и т. д. все с минусом
    /// 
    /// </summary>
    /// <param name="changeHealth"></param>
    public void SetCurrentHealth(int changeHealth)
    {
        if(isDead)
        {
            return;
        }

        curretnHealth += changeHealth;



        //если нет жизни то метод смерти
        if(curretnHealth <= 0)
        {
            Death();
        }

        //проверка чтобы уровень жизни не вылез за пределы
        else if(curretnHealth >= maxHealth)
        {
            curretnHealth = maxHealth;
        }


        ///если пришло отрицательное значение тоесть это урон а не добавление жизни
        ///
        if(changeHealth < 0)
        {
            isDamage = true;
            //StartCoroutine(HolderIsDamage());
        }
    }

    public IEnumerator HolderIsDamage()
    {
        isDamage = true;

        yield return new WaitForSeconds(.5f);

        isDamage = true;
    }

    //типа сэттэр и гэттэр
    public int GetMaxHealth()
    {
        return maxHealth;
    }
    public int GetCurrentHealth()
    {
        return curretnHealth;
    }




    public void Attaka()
    {
        //если атакуем то вторую атаку не производим
        if(isAttack)
        {
            return;
        }

        _animator.SetTrigger("Attacka");

        StartCoroutine(axeManager.Attack());


        StartCoroutine(Attack());
    }

    private void Move()
    {
        //если умер
        if(isDead)
        {
            return;
        }

       

        //в этом методе почти все по стандарту
        // в общем как у всех
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");



        Vector3 camForward_Dir = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 move = v * camForward_Dir + h * Camera.main.transform.right;

        if(move.magnitude > 1f)
        {
            move.Normalize();
        }


        move = transform.InverseTransformDirection(move);


        float turnAmount = Mathf.Atan2(move.x, move.z);

        transform.Rotate(0, turnAmount * RotationSpeed * Time.deltaTime, 0);

        if(_characterController.isGrounded)
        {
            //_animator.SetBool("run", move.magnitude > 0);

            _moveDir = transform.forward * move.magnitude;

            _moveDir *= Speed;

        }

        _moveDir.y -= Gravity * Time.deltaTime;

        _characterController.Move(_moveDir * Time.deltaTime);

        //если есть есть хоть какаято скорость вперед то анимируем ходьбой
        if(move.z != 0)
        {
            //если по герою попали и нанесли урон 
            //то таким ифом можно вкличить анимацию damage
            //иначе все анимации перехватываем либ Run либо Idle
            //а Damage просто не успевает включаться
            if(isDamage)
            {
                SetAnim(EStateAnimMonster.Damage);
                isDamage = false;
                return;
            }

            SetAnim(EStateAnimMonster.Run);
        }
        else
        {
            if(isDamage)
            {
                SetAnim(EStateAnimMonster.Damage);
                isDamage = false;
                return;
            }

            SetAnim(EStateAnimMonster.Idle);
        }
    }

    /// <summary>
    /// /Не обращаем внимания на этот закоментироваанный метод
    /// </summary>
    /// <returns></returns>
    //void Move()
    //{

    //    if(isAttack)
    //    {
    //        return;
    //    }
    //    if(characterController.isGrounded)
    //    {
    //        move = new Vector3(0.0f, 0.0f, Input.GetAxis("Vertical"));
    //        move = transform.TransformDirection(move);
    //        move *= speedCorrection;
    //    }
    //    move.y -= gravity * Time.deltaTime;
    //    characterController.Move(move * Time.deltaTime);


    //    if(move.z != 0)
    //    {
    //        SetAnim(EStateAnimMonster. Run);
    //    }
    //    else
    //    {
    //        SetAnim(EStateAnimMonster. Idle);
    //    }

    //    transform.Rotate(transform.up, rotationValue * Input.GetAxis("Horizontal") * Time.deltaTime);
    //}


    //при атаке делаем  isAttack = true;
    //чтобы другие мотоды знали что герой атакует и 

    public IEnumerator Attack()
    {
        isAttack = true;

        yield return new WaitForSeconds(2.5f);

        isAttack = false;
    }
    /// <summary>
    /// Смерть монстра
    /// </summary>
    public void Death()
    {
        isDead = true;
        Debug.Log("Death");
        //номера в перечисления срвпадают со значениями в переходах в аниматор контроллере 
        //(4 это переход к анимации смерти)
        SetAnim(EStateAnimMonster.Die);
        
        //вызываем корутину (для задержки перед удалением) чтобы
        //анимация смерти успела проиграть.
        // можно было сделать событиями в анимации (в самом клипе)
        StartCoroutine(CorytDeath());

    }

    /// <summary>
    /// Метод изменения анимации
    /// </summary>
    public void SetAnim(EStateAnimMonster eStateAnimMonster)
    {
        _animator.SetInteger("StateAnim", (int)eStateAnimMonster);
    }

    //для задержки перед удалением героя
    IEnumerator CorytDeath()
    {
        yield return new WaitForSeconds(5);

        uiPanel.GameOver();

        HealthMonster[] hM = FindObjectsOfType<HealthMonster>();

        for(int i = 0; i < hM.Length; i++)
        {

            //герой умер а значит и удаляем индикаторы здоровья у монстров 
            hM[i].DestroyHealthBar(false);
        }

        //удаляем героя при смерти
        Destroy(gameObject);
    }

    //для уничтожения "магических чисел". Здесь каждое значение 
    //совпадает со значением перехода в аниматор контроллере.
    //напрример: для перехода в Run нажно поменять значение 
    //в аниматор контроллере на  и т. д.
    public enum EStateAnimMonster
    {
        Idle = 0,
        Run = 1,
        Attack = 2,
        Damage = 3,
        Die = 4
    }
}
