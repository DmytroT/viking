﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeManager : MonoBehaviour
{
    [SerializeField]
    Transform axeDamagePoint;

    [Header("отрицательное - отнимает здоровье;положительное - прибавляет здоровье;")]
    [SerializeField]
    int axeDamege = -1;

    [SerializeField]
    AudioSource audioSource;
    public IEnumerator Attack()
    {
        yield return new WaitForSeconds(.8f);

        //включеаем триггер
        axeDamagePoint.GetComponent<BoxCollider>().enabled = true;

        //Collider[] colliders = Physics.OverlapSphere(axeDamagePoint.transform.position, 1f);

        //Debug.Log("``1f");

        //foreach(Collider item in colliders)
        //{
        //    if(item.GetComponent<Monster>())
        //    {
        //        item.GetComponent<Monster>().SetCurrentHealth(axeDamege);
        //    }
        //}

        yield return new WaitForSeconds(1f);
        //Debug.Log("1.5f");
        //////////////////////////////////////////////
        ///
        /// //выключеаем триггер
        axeDamagePoint.GetComponent<BoxCollider>().enabled = false;


    }

    /// <summary>
    /// этим триггер энтером и наносим урон
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("123");

        if(other.GetComponent<Monster>())
        {
            Debug.Log("Monster123");

            audioSource.Play();

            other.GetComponent<Monster>().SetCurrentHealth(axeDamege);

            other.GetComponent<Monster>().SetActiveSplashBlood();
        }
    }
}
