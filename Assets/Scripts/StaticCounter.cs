﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//сколько монстров поубивали
//статиком наверно лучше всего
public class StaticCounter
{
    [SerializeField]
    private static int counter;


    public static void SetCounter(int value)
    {
        counter += value;
    }

    public static int GetCounter()
    {
        return counter;
    }

    public static void Reset()
    {
        counter = 0;
    }
}
