﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// отвечает за атаку монстра
/// //этот скрипт висит на точке (EmptyObject)
/// на руке монстра в скелете в иерархии можно найти
/// и работает как и  у героя
/// триггер включается и отключается в нужный момент
/// и еслив триггер попал герой то герою урон
/// </summary>
public class PointDamage : MonoBehaviour
{
    [SerializeField]
    Transform damagePoint;

    [Header("отрицательное - отнимает здоровье;положительное - прибавляет здоровье;")]
    [SerializeField]
    int damege = -1;

    public IEnumerator Attack()
    {
        yield return new WaitForSeconds(.8f);


        damagePoint.GetComponent<BoxCollider>().enabled = true;

      

        yield return new WaitForSeconds(1f);
        //Debug.Log("1.5f");
        //////////////////////////////////////////////
        damagePoint.GetComponent<BoxCollider>().enabled = false;


    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("123");

        if(other.GetComponent<Viking>())
        {

            Debug.Log("Попали по викингу");


            other.GetComponent<Viking>().SetCurrentHealth(damege);
        }
    }
}
