﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


/// <summary>
/// 
/// </summary>
public class UIManager : MonoBehaviour
{
    // [SerializeField]
    // Animator cameraAnimator;

    [SerializeField]
    GameObject panelGame;

    [SerializeField]
    GameObject panelGameOver;

    [SerializeField]
    GameObject cameraFly;

    [SerializeField]
    GameObject cameraHero;

    [SerializeField]
    Text textScoreGame;

    [SerializeField]
    Text textScoreGameOver;

    [SerializeField]
    Transform startPoint;

    [SerializeField]
    GameObject player;

    [SerializeField]
    Spawner spawner;


    public void StartGame()
    {
        Time.timeScale = 1;
        cameraHero.SetActive(true);
        cameraFly.SetActive(false);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        //cameraAnimator.SetInteger("CameraGame", 1);
        spawner.InstantiateMonster();
    }

    private void Start()
    {
        Time.timeScale = 0.0f;
    }

    public void GameOver()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        cameraFly.SetActive(true);
        cameraHero.SetActive(false);



        panelGame.SetActive(false);
        panelGameOver.SetActive(true);

        textScoreGameOver.text = StaticCounter.GetCounter().ToString();
    }

    private void Update()
    {
        textScoreGame.text = StaticCounter.GetCounter().ToString();
    }


    public void Restart()
    {
        Instantiate(player, startPoint.transform.position, Quaternion.identity);

        StaticCounter.Reset();

        cameraFly.SetActive(false);
        cameraHero.SetActive(true);

        panelGame.SetActive(true);
        panelGameOver.SetActive(false);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;


        //
        spawner.SpawnerRestart();

        spawner.InstantiateMonster();
    }


  

    /// <summary>
    /// Выход из игры
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }
}
